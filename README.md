# Site Web Personnel

[![demo](https://img.shields.io/badge/theme-demo-brightgreen.svg)](https://alshedivat.github.io/al-folio/)
[![license](https://img.shields.io/github/license/mashape/apistatus.svg?maxAge=2592000)](https://github.com/alshedivat/al-folio/blob/master/LICENSE)

Simple site web fait avec [Jekyll](https://jekyllrb.com/) un thème académique, basé sur [al-folio](https://github.com/alshedivat/al-folio).

## License

MIT
