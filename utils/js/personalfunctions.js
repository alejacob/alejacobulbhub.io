function update()
{
	var jours = new Array(8); 
	jours[1] = "Dimanche";
	jours[2] = "Lundi"; jours[3] = "Mardi"; 
	jours[4] = "Mercredi"; jours[5] = "Jeudi";
	jours[6] = "Vendredi"; jours[7] = "Samedi";

	var mois = new Array(13);
	mois[1] = "Janvier"; mois[2] = "Février"; 
	mois[3] = "Mars"; mois[4] = "Avril";
	mois[5] = "Mai"; mois[6] = "Juin";
	mois[7] = "Juillet"; mois[8] = "Août";
	mois[9] = "Septembre"; mois[10] = "Octobre";
	mois[11] = "Novembre"; mois[12] = "Decembre";

	var date = new Date(document.lastModified)
	var jour_update = jours[date.getDay() + 1]
	var mois_update = mois[date.getMonth() + 1]
	var date_update = date.getDate()
	var annee = date.getYear()
	var heure = date.getHours();
	var min = date.getMinutes();
	if (annee < 2000) 
		annee = annee + 1900
	document.writeln("Last Update : " + jour_update + " " + date_update + " " + mois_update + " " + annee + " at " +heure+":"+min)
}
